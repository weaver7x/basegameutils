# ** Base Game Utils** #

Copyright (C) 2015 Google Inc.

Biblioteca Android que simplifica la integración de Google Play Games en aplicaciones Android.

Código fuente obtenido de https://github.com/playgameservices/android-basic-samples.

Depende de la biblioteca genérica de Android **Google Play Services** de Google Inc.



# ** Changes** #
APP_STATE comentado para que no de problemas al referenciar con google_play_services.

android:minSdkVersion="11"




# ** Desde** #

6 de julio de 2015